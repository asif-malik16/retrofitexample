package com.sslwireless.retrofitexample.model;

/**
 * Created by Priyanka on 7/6/2017.
 */

public class Info {
    public String name;
    public String age;

    public String getName() {
        return name;
    }

    public String getAge() {
        return age;
    }
}
