package com.sslwireless.retrofitexample.api;

import com.sslwireless.retrofitexample.model.Info;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.POST;

/**
 * Created by Priyanka on 7/6/2017.
 */

public interface ApiInterface {
    @POST("read_data.php")
    Call<List<Info>> getInfo();
}
