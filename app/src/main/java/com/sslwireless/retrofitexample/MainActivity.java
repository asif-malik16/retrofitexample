package com.sslwireless.retrofitexample;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;
import android.text.format.DateFormat;

import com.sslwireless.retrofitexample.api.ApiClient;
import com.sslwireless.retrofitexample.api.ApiInterface;
import com.sslwireless.retrofitexample.model.Info;

import java.util.Calendar;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity implements DatePickerDialog.OnDateSetListener, TimePickerDialog.OnTimeSetListener {

    private List<Info> infoList;
    private ApiInterface apiInterface;
    private TextView name, age, textView;
    private Button pick_date_time;
    private int day, month, year, hour, minute, dayFinal, monthFinal, yearFinal,
            hourFinal, minuteFinal;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        name = (TextView) findViewById(R.id.name);
        age = (TextView) findViewById(R.id.age);
        textView = (TextView) findViewById(R.id.textView);
        pick_date_time = (Button) findViewById(R.id.pick_date_time);

        pick_date_time.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Calendar c = Calendar.getInstance();
                year = c.get(Calendar.YEAR);
                month = c.get(Calendar.MONTH);
                day = c.get(Calendar.DAY_OF_MONTH);

                DatePickerDialog datePickerDialog = new DatePickerDialog(MainActivity.this, MainActivity.this, year, month, day);
                datePickerDialog.show();
            }
        });

        apiInterface = ApiClient.getApiClient().create(ApiInterface.class);

        Call<List<Info>> call = apiInterface.getInfo();
        call.enqueue(new Callback<List<Info>>() {
            @Override
            public void onResponse(Call<List<Info>> call, Response<List<Info>> response) {
                infoList = response.body();
                Spinner s = (Spinner) findViewById(R.id.spinner);

                for (int i = 0; i < infoList.size(); i++) {

                    String names = infoList.get(i).getName();
                    Log.d("TAG", "List data is: " + names);

                    name.setText(names);

                    String ages = infoList.get(i).getAge();
                    Log.d("TAG", "List data is: " + ages);

                    age.setText(ages);
                }

                ArrayAdapter<String> adapter = new ArrayAdapter<String>(getApplicationContext(),
                        android.R.layout.simple_spinner_item) {

                    @Override
                    public View getView(int position, View convertView, ViewGroup parent) {

                        View v = super.getView(position, convertView, parent);
                        if (position == getCount()) {
                            ((TextView) v.findViewById(android.R.id.text1)).setText("");
                            ((TextView) v.findViewById(android.R.id.text1)).setHint(getItem
                                    (getCount())); //"Hint to be displayed"
                        }

                        return v;
                    }

                    @Override
                    public int getCount() {
                        return super.getCount() - 1; // you dont display last item. It is used as
                        // hint.
                    }

                };

                for (int i = 0; i < infoList.size(); i++) {
                    adapter.add(infoList.get(i).getName());
                }
                adapter.add("Select your choice");
                adapter.setDropDownViewResource(R.layout.spinner_dropdown_item);
                s.setAdapter(adapter);
                s.setSelection(adapter.getCount());
            }

            @Override
            public void onFailure(Call<List<Info>> call, Throwable t) {

            }
        });
    }

    @Override
    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
        yearFinal = year;
        monthFinal = month;
        dayFinal = dayOfMonth;

        Calendar c = Calendar.getInstance();
        hour = c.get(Calendar.HOUR_OF_DAY);
        minute = c.get(Calendar.MINUTE);

        TimePickerDialog timePickerDialog = new TimePickerDialog(MainActivity.this, MainActivity.this, hour, minute, DateFormat.is24HourFormat(this));
        timePickerDialog.show();
    }

    @Override
    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
        hourFinal = hourOfDay;
        minuteFinal = minute;

        textView.setText("Year: " + yearFinal + "\n" + "Month: " + monthFinal + "\n" + "day: " + dayFinal + "\n" + "Hour: " + hourFinal + "\n" + "Minute: " + minuteFinal);
    }
}
